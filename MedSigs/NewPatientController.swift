//
//  NewPatientController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class NewPatientController : UITableViewController{

    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func cancelButton(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "unwindToPatientSegue") {
            let dest = segue.destinationViewController as? PatientsTableViewController
            dest!.new_patient  = emailTextField.text!
        }
    }
    
   
}
