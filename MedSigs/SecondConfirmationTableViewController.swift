//
//  SecondConfirmationTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/5/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class SecondConfirmationTableViewController : UITableViewController
{
    
    var orig_sig: Sig?
    var flag: String?
    
    
    var properties = [[(String, String)]]()
    var sections = [String]()
    

    
    override func viewDidLoad() {
        tableView.rowHeight=SecondConfirmationTableViewCell.height
        super.viewDidLoad()
        self.loadSig()
    }
    
    
    func loadSig()
    {
       
    }
    
    
    func loadSigCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        if error==nil{
            var i = 0
            print("Loading the sig")
            print(responseObject)
            orig_sig?.update_sig( (responseObject!["sol"]![0]["cf"]!! as! [NSDictionary])[0]  )
            for q in (responseObject!["sol"]![0]["cf"] as! NSArray)
            {
                let q_prime = q as! [String: AnyObject]
                
                for (x,y) in q_prime{
                    
                    properties.append([(String,String)]())
                    sections.append(x)
                    
                    for a in (y as? NSArray)!{
                        for (key,value) in (a as?NSDictionary)!
                        {
                            if !(value is NSNull){
                                if(value is NSNumber)
                                {
                                    properties[i].append((key as! String, value.stringValue))
                                }
                                else{
                                    properties[i].append((key as! String, value as! String))
                                }
                            }
                            
                        }
                    }
                    properties[i].append(("Add New", " "))
                    i++
                }
                
            }
            
            tableView.reloadData()
        }
        else
        {
            print("Error in API Call")
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return properties[section].count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "SecondConfirmationTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! SecondConfirmationTableViewCell
        
        
        let prop = properties[indexPath.section][indexPath.row]
        
        cell.nameLabel.text=prop.0
        cell.valueLabel.text=prop.1
        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return self.sections[section]
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier=="unwindToPatientDetailSecond"{
            let x = segue.destinationViewController as! PatientDetailTableViewController
            x.new_sig=self.orig_sig
        }
    }

}
