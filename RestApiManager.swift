//
//  RestApiManager.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/9/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit
import Alamofire



enum ReqPrefix
{
    case Get
    case Update
    case Insert
    case Remove
    case Create
    case User

}

enum ReqPath
{
    case UserData
    case DoctorData
    case DoctorPatient
    case PatientData
    case PatientDoctor
    case PatientPreference
    case PatientPrescription
    case PatientSchedule
    case PrescriptionData
    case PrescriptionDetail
    case PrescriptionPreference
    case TimeData
    case UserCreate
    case UserLogin

}


class RestApiManager: NSObject {
    static let sharedInstance = RestApiManager()
    
    let baseAwsUrl="http://ec2-52-35-158-42.us-west-2.compute.amazonaws.com:5000/"
    
    
    func getPrefix(prefixType: ReqPrefix) -> String
    {
        switch prefixType
        {
            case .Get:
                return "get/"
            case .Update:
                return "update/"
            case .Insert:
                return "insert/"
            case .Remove:
                return "remove/"
            case .Create:
                return "create/"
            case .User:
                return "user/"
        }
        
    }
    
    
    func getPath(pathType: ReqPath) -> String
    {
        switch pathType
        {
        case .UserData:
            return "user/data"
        case .DoctorData:
            return "doctor/data"
        case .DoctorPatient:
            return "doctor/patient"
        case .PatientData:
            return "patient/data"
        case .PatientDoctor:
            return "patient/doctor"
        case .PatientPreference:
            return "patient/preference"
        case .PatientPrescription:
            return "patient/prescription"
        case .PatientSchedule:
            return "patient/schedule"
        case .PrescriptionData:
            return "prescription/data"
        case .PrescriptionDetail:
            return "prescription/detail"
        case .PrescriptionPreference:
            return "prescription/preference"
        case .TimeData:
            return "time/data"
        case .UserCreate:
            return "create"
        case .UserLogin:
            return "login"
        
        }
        
    }
    
    func makeRequest(completionHandler: (NSDictionary?, NSError?, [String:AnyObject]) -> (), prefixType :ReqPrefix , pathType : ReqPath,args: (String,AnyObject)...){
        var params = [String:AnyObject]()
        for arg in args{
            params[arg.0]=arg.1
        }
        
        let urlEnding = getPrefix(prefixType) + getPath(pathType)
        makePostCall(urlEnding, parameters: params, completionHandler: completionHandler)
        
        
    }
    
    func makeRequest(completionHandler: (NSDictionary?, NSError?, [String:AnyObject]) -> (), prefixType :ReqPrefix , pathType : ReqPath,args: [String:AnyObject]){
        let urlEnding = getPrefix(prefixType) + getPath(pathType)
        makePostCall(urlEnding, parameters: args, completionHandler: completionHandler)
        
    }
    
    func makePostCall(section: String, parameters: [String:AnyObject], completionHandler: (NSDictionary?, NSError?, [String:AnyObject]) -> ()) {
        print(baseAwsUrl+section)
        Alamofire.request(.POST, baseAwsUrl+section, parameters: parameters, encoding: .JSON)
            .responseJSON { response in
                switch response.result {
                case .Success(let value):
                    completionHandler(value as? NSDictionary, nil, parameters)
                case .Failure(let error):
                    completionHandler(nil, error, parameters)
                }
        }
    }
}




