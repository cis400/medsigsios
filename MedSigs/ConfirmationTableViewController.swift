//
//  ConfirmationTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class ConfirmationTableViewController : UITableViewController, UITextFieldDelegate
{
    
    //MARK: Properties
    var startDate : NSDate?
    var sig_text : String = ""
    var patient: Patient?
    var sig : Sig?
    var second_sig : Sig?
    var flag = "0"
    var temp_change : (String, String, String)?
    var isNoise = false;
    var arrayOfValues : [String] = [String]()
    var components = [[SigComponent]]()
    var sections = [(String,String)]()
    var rowBeingEdited : Int? = nil
    var isEdit = false
    var isLoading = false;
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var trashButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight=SigTableViewCell.height
        if(isEdit)
        {
            
            reloadLocalSig()
        }
        else
        {
            self.loadSig()
        }
        hideKeyboardWhenTappedAround()
        self.tableView.tableFooterView = UIView()
    }
    
    
    func loadSig()
    {
        RestApiManager.sharedInstance.makeRequest(loadSigCallBack, prefixType: ReqPrefix.Create, pathType: ReqPath.PrescriptionData, args: ("med_sig",sig_text), ("start_date", DateTimeManager.instance.convertToServerStringWhole(self.startDate!)))
    }
    
    func reloadLocalSig()
    {
        self.sections.removeAll()
        self.components.removeAll()
        let (sections, components) = (sig?.get_table_info())!
        self.sections.appendContentsOf(sections)
        self.components.appendContentsOf(components)
        tableView.reloadData()
    }
    
    func loadSigCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        print(responseObject)
        if error==nil{
            print("Loading the sig**********")
            
            self.flag = responseObject!["sol"]![0]["flag"] as! String
            if(self.flag != "0")
            {
                self.saveButton.title="Next"
            }
            
            self.sig = Sig(dic : (responseObject!["sol"]![0]["cf"]!! as! [NSDictionary])[0])
            
            
            let (sections, components) = (sig?.get_table_info())!
            self.sections.appendContentsOf(sections)
            self.components.appendContentsOf(components)
            
            sig!.sig_dict["med_sig"]=self.sig_text
            
            sig!.sig_dict["doctor_id"]=GlobalState.globalState.doctor_id
            print("inserting the patient_id")
            print(self.patient?.patient_id)
            sig!.sig_dict["patient_id"]=self.patient?.patient_id
            
            
            tableView.reloadData()
        }
        else
        {
            print("Error in API Call")
        }
    }
    
    
    func loadSecondSig()
    {
        isNoise=true;
        self.saveButton.enabled = false
        self.isLoading = true
        tableView.reloadData()
        RestApiManager.sharedInstance.makeRequest(loadSecondSigCallBack, prefixType: ReqPrefix.Create, pathType: ReqPath.PrescriptionDetail, args: ("flag",self.flag))
        
    }
    
    func loadSecondSigCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        self.isLoading = false
        self.saveButton.title="Save"
        self.saveButton.enabled = true
        print("******Resposne*******")
        print(responseObject)
        print("******Resposne*******")
        if error==nil{
            
            self.second_sig = Sig(dic : (responseObject!["sol"]![0]["cf"]!! as! [NSDictionary])[0])
            let (sections, components) = (second_sig?.get_table_info())!
            self.sections.removeAll()
            self.components.removeAll()
            self.sections.appendContentsOf(sections)
            self.components.appendContentsOf(components)
            tableView.reloadData()
        }
        else
        {
            
            print("Error in API Call")
        }
    }
    
    
    func addComponent(category: String, field: String, val: AnyObject)
    {
        sig?.modify_bottom(category, key2: field, val: val)
        self.reloadLocalSig()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return components[section].count
    }
    
    
    
    @IBAction func addButtonPressed(sender: AnyObject) {
        performSegueWithIdentifier("segueToAdd", sender: self)
    }
    
    func boolSwitchChanged(sender : ConfirmationUISwitch)
    {
        let (key1, key2) = sender.keys!
        if sender.on
        {
            self.sig!.modify_bottom(key1, key2: key2, val: "True")
        }
        else{
            self.sig!.modify_bottom(key1, key2: key2, val: "False")
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "SigTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! SigTableViewCell
        
        
        
        let comp = components[indexPath.section][indexPath.row]
      
            
            if(comp.type == ValueType.Bool)
            {
                cell.boolSwitch.hidden=false
                cell.valueTextField.hidden=true
                cell.boolSwitch.keys=(comp.category!, comp.key_string!)
                cell.boolSwitch.addTarget(self, action: "boolSwitchChanged:", forControlEvents: UIControlEvents.ValueChanged)
                cell.boolSwitch.on=comp.value_bool!
            }
            else{
                cell.boolSwitch.hidden=true
                cell.valueTextField.hidden=false
                if(comp.type == ValueType.Number)
                {
                    cell.valueTextField.keyboardType = .DecimalPad
                }
                else
                {
                    cell.valueTextField.keyboardType = .Default
                }
                cell.valueTextField.text=comp.value_text
                cell.valueTextField.keys=(comp.category!, comp.key_string!)
                cell.valueTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
                cell.valueTextField.delegate=self
            }
            cell.accessoryType = .None
            cell.selectionStyle = .None
            cell.nameLabel.text=comp.title_text
            
        
        
        return cell
    }
    
    func tryUpdate(sig :Sig,key1 : String,  key2: String, value : String){
        var val : AnyObject?
        switch (sig.type_dict[key1+key2])!{
        case .Bool:
            val = (value == "true")
            break
        case .Number:
            val = NSNumber(double: Double(value)!)
            break
        case .String:
            val = value
            break
        }
        sig.modify_bottom(key1, key2: key2, val: val!)
    }
    
    func textFieldDidChange(textField: UITextField)
    {
        let textField = textField as! ConfirmationUITextField
        let value = textField.text!
        let (key1, key2) = textField.keys!
        self.temp_change = (key1, key2, value)
        
    }
    
    func textFieldDidEndEditing(textField: UITextField){
        let textField = textField as! ConfirmationUITextField
        let value = textField.text!
        let (key1, key2) = textField.keys!
        self.tryUpdate(self.sig! ,key1: key1, key2: key2, value: value)
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
       
            return self.sections[section].0
        
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
      
            return sections.count
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        if let t_c = self.temp_change
        {
            self.tryUpdate(self.sig!, key1: t_c.0, key2: t_c.1, value: t_c.2)
        }
        
        if(isEdit)
        {
            performSegueWithIdentifier("segueUnwindToUpdate", sender: self)
        }
        else{
            if !isNoise
            {
                if self.flag == "0"
                {
                    performSegueWithIdentifier("unwindToPatientDetail", sender: self)
                }
                else
                {
                    self.sections.removeAll()
                    self.components.removeAll()
                    self.loadSecondSig()
                }
            }
            else
            {
                self.sig?.update_sig((second_sig?.sig_dict)!)
                performSegueWithIdentifier("unwindToPatientDetail", sender: self)
            }
        }
    }
    
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier=="unwindToPatientDetail" || segue.identifier=="segueUnwindToUpdate"{
            let x = segue.destinationViewController as! PatientDetailTableViewController
            x.new_sig=self.sig!
            if(isEdit){isEdit=false}
        }
        if segue.identifier=="segueToAdd"
        {
            let x = segue.destinationViewController as! UINavigationController
            let y = x.viewControllers[0] as! ConfirmationAdjustmentViewController
            y.sig = self.sig
            y.category = [ ("dose","dose"), ("duration","duration"), ("event", "event"), ("frequency", "frequency"), ("medication", "medication"), ("method","method"), ("span","span")]
        }
    }
    
    @IBAction func unwindToConfirmation(segue: UIStoryboardSegue){
        
        
    }
    
}
