//
//  MedSigsTests.swift
//  MedSigsTests
//
//  Created by Brenden Guthrie on 2/3/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import XCTest


@testable import MedSigs

class MedSigsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPrescriptionObject(){
        
        var sig = "[{\"event\": [{\"cond\": null, \"event\": \"PRN\"}], \"medication\": [{\"scientific_unit2\": null, \"name\": \"ACETAMINOPHEN\", \"sci_quantity2\": null, \"scientific_unit\": \"MILLIGRAM\", \"sci_quantity\": 400.0, \"item_unit\": \"TABLET\", \"name2\": null, \"conc_num_unit\": null, \"conc_den_quantity\": null, \"item_quantity\": null, \"conc_num_quantity\": null, \"conc_den_unit\": null}], \"span\": [{\"begin\": null, \"end\": null}], \"dose\": [{\"sci_quantity\": 800.0, \"item_quantity\": 2.0, \"item_unit\": \"TABLET\", \"scientific_unit\": \"MILLIGRAM\"}], \"frequency\": [{\"multiple\": null, \"max_multiple\": null, \"max_quantity\": null, \"min_multiple\": null, \"min_quantity\": null, \"unit\": \"HOUR\", \"quantity\": 4.0}], \"duration\": [{\"unit\": null, \"quantity\": null}], \"method\": [{\"how\": null, \"route\": null}]}]"
            
            do {
                print("Loading the sig")
                let data: NSData = sig.dataUsingEncoding(NSUTF8StringEncoding)!
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                
                for (x,y) in (json[0] as? [String: AnyObject])! {
                    print(x)
                    //xprint(y)
                    for a in (y as? NSArray)!{
                        for (key,value) in (a as?NSDictionary)!
                        {
                            print(key, value)
                        }
                    }
                    
                }
            } catch {
                print("error serializing JSON: \(error)")
            }
        }
    
    
}
