//
//  PhysiciansTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/9/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class PhysiciansTableViewController: UITableViewController{
    //@IBOutlet weak var menuButton:UIBarButtonItem!
    
    var doctors = [Doctor]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadDoctors()
        
        if self.revealViewController() != nil {
            
            self.navigationItem.leftBarButtonItem!.target = self.revealViewController()
            self.navigationItem.leftBarButtonItem!.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        tableView.rowHeight=90
        self.tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadDoctors(){
        
        //Real API Call
        RestApiManager.sharedInstance.makeRequest(addDoctors, prefixType: ReqPrefix.Get , pathType: ReqPath.PatientDoctor, args: ("patient_id",GlobalState.globalState.patient_id))
    }
    
    func addDoctors(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        // use responseObject and error here
        print("responseObject = \(responseObject); error = \(error)")
        if(error==nil){
                for x in (responseObject?.objectForKey("sol")) as! NSArray{
                    self.doctors.append(Doctor(arr: x))
                }
        }
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctors.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "PhysiciansTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PhysiciansTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        let doc = doctors[indexPath.row]
        
        cell.nameLabel.text=doc.name
        return cell
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
   
    
}

