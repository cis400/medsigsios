//
//  NewPrescriptionController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class NewPrescriptionController : UIViewController{
    
    var patient : Patient?
    var startDate : NSDate?
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        startDate = datePicker.date
    }
    @IBOutlet weak var sigTextField: UITextField!
    
    @IBAction func datePickerChanged(sender: AnyObject) {
        
        startDate = datePicker.date
        
    }
    
    
    @IBAction func convertButtonPressed(sender: AnyObject) {
        performSegueWithIdentifier("moveToConfirmationSegue", sender: sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func cancelButtonPressed(sender: AnyObject) {
          dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func unwindPrescriptions(sender: UIStoryboardSegue) {
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier=="moveToConfirmationSegue"
        {
        
            let dest_nav = segue.destinationViewController as! UINavigationController
            let dest = dest_nav.viewControllers[0] as! ConfirmationTableViewController
            dest.sig_text=sigTextField.text!
            dest.startDate = self.startDate
            dest.patient=self.patient
        }
    }
}

