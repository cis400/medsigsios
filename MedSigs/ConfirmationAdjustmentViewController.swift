//
//  ConfirmationAdjustmentTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class ConfirmationAdjustmentViewController : UIViewController,UIPickerViewDataSource, UIPickerViewDelegate
{
    var sig : Sig?
    var curr_category : String?
    var pickerDataSource = [(String, ValueType)]();
    
    var category : [(String, String)]?
    var component : SigComponent?
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var adjustmentPicker: UIPickerView!
    @IBOutlet weak var boolSegmentControl: UISegmentedControl!
    override func viewDidLoad() {
        
        self.title="Add Field"
        
        adjustmentPicker.dataSource = self
        adjustmentPicker.delegate = self
        var i = 0
        print(category)
        while(pickerDataSource.isEmpty){
            print(category![i])
            if let list = sig?.getMissingValues(category![i].0){
                pickerDataSource.appendContentsOf(list)
                adjustmentPicker.selectRow(i, inComponent: 0, animated: false)
            }
            i++
        }
        self.adjust_text_field(adjustmentPicker.selectedRowInComponent(1))
        print("Contents of pickerDataSource")
        print(category)
        print(pickerDataSource)
        
        super.viewDidLoad()
    }
    
    @IBAction func addButtonPressed(sender: AnyObject) {
        performSegueWithIdentifier("unwindToConfirmation", sender: sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component==0
        {
            return (self.category?.count)!
        }
        else{
            return self.pickerDataSource.count
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0
        {
            return category![row].0
        }
        else
        {
            return pickerDataSource[row].0
        }
    }
    
    
    func adjust_text_field(row : Int)
    {
        print(row)
        print(pickerDataSource)
        if !pickerDataSource.isEmpty
        {
            if(pickerDataSource[row].1==ValueType.Bool)
            {
                boolSegmentControl.hidden=false
                valueTextField.hidden=true
            }
            else if(pickerDataSource[row].1==ValueType.Number)
            {
                boolSegmentControl.hidden=true
                valueTextField.hidden=false
                valueTextField.keyboardType = .DecimalPad
            }
            else
            {
                boolSegmentControl.hidden=true
                valueTextField.hidden=false
                valueTextField.keyboardType = .Default
            }
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component==0
        {
            self.pickerDataSource.removeAll()
            if let list = sig?.getMissingValues(category![row].0){
                pickerDataSource.appendContentsOf(list)
            }
            adjustmentPicker.reloadComponent(1)
            if !pickerDataSource.isEmpty{
                self.adjust_text_field(0)
            }
        }
        else
        {
            self.adjust_text_field(row)
        }
        
        
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "unwindToConfirmation"
        {
            let x = segue.destinationViewController as! ConfirmationTableViewController
            let key1 = self.category?[adjustmentPicker.selectedRowInComponent(0)].0
            let key2 = pickerDataSource[adjustmentPicker.selectedRowInComponent(1)].0
            switch pickerDataSource[adjustmentPicker.selectedRowInComponent(1)].1
            {
            case .Bool:
                print("Modifying a bool")
                let q = (boolSegmentControl.selectedSegmentIndex==0)
                if q
                {
                     x.addComponent(key1!, field: key2, val: "True")
                }
                else
                {
                     x.addComponent(key1!, field: key2, val: "False")
                }
                
               
                break
            case .Number:
                x.addComponent(key1!, field: key2, val:  NSNumber(double: Double(valueTextField.text!)!))
                break
            case .String:
                x.addComponent(key1!, field: key2, val:  valueTextField.text!)
                break
            }
            
        }
    }
    
}
