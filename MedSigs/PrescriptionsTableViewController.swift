//
//  PrescriptionsTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/9/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class PrescriptionsTableViewController: UITableViewController{
    
    //MARK: Properties
    
    @IBOutlet weak var editButton: UIBarButtonItem!
    var sigs = [Int : Sig]()
    var prescriptions = [Prescription]()
    var doctors = [Int: String]()
    var areEditing: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "PrescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "PrescriptionTableViewCell")
        tableView.rowHeight=PrescriptionsTableViewCell.height
        self.areEditing = false;
        self.editButton.title="Edit"
        loadPrescriptions()
        
        if self.revealViewController() != nil {
            
            self.navigationItem.leftBarButtonItem!.target = self.revealViewController()
            self.navigationItem.leftBarButtonItem!.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        let longpress = UILongPressGestureRecognizer(target: self, action: "longPressGestureRecognized:")
        tableView.addGestureRecognizer(longpress)
        self.tableView.tableFooterView = UIView()
      
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
   
    
    func loadPrescriptions(){
        RestApiManager.sharedInstance.makeRequest(loadPrescriptionsCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.PatientPrescription, args: ("patient_id", (GlobalState.globalState.patient_id)))
    }
    
    func loadPrescriptionsCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        // use responseObject and error here
        //print("responseObject = \(responseObject); error = \(error)")
        if(error==nil){
            for x in (responseObject?.objectForKey("sol")) as! NSArray{
                    prescriptions.append(Prescription(arr: x))
                }
        }
        
        self.loadSigs()
        for pres in prescriptions{
            if doctors[pres.doctor_id] == nil{
                RestApiManager.sharedInstance.makeRequest(addDoctorCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.DoctorData, args: ("doctor_id", pres.doctor_id))
            }
        
        }
        
    }
    
    func try_table_reload()
    {
        if(sigs.count==prescriptions.count)
        {
            tableView.reloadData()
        }
    }
    
    func addDoctorCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        if(error==nil)
        {
            let dict = (responseObject?.objectForKey("sol")! as! [NSDictionary])[0]
            doctors.updateValue((dict["doctor_name"] as! String), forKey: (parameters["doctor_id"] as! Int))
        }
        try_table_reload()
        
    }
    
    
    func loadSigs()
    {
        for x in prescriptions{
            RestApiManager.sharedInstance.makeRequest(loadSigsCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.PrescriptionData, args: ("prescription_id",x.prescription_id ))
        }
    }
    
    func loadSigsCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        print(responseObject)
        if error==nil
        {
            let temp_sig = Sig(dic : (responseObject!["sol"]! as! [NSDictionary])[0] )
            temp_sig.detail_init( (responseObject!["sol"]!  as! [NSDictionary])[0])
            self.sigs.updateValue(temp_sig, forKey: (parameters["prescription_id"] as! Int))
        }
        try_table_reload()
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if(!areEditing)
        {
            performSegueWithIdentifier("segueToPrescriptionDetail", sender: indexPath)
        }
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sigs.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "PrescriptionTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PrescriptionsTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        let pres = prescriptions[indexPath.row]
        let sig = sigs[pres.prescription_id]
        let doctor_name = doctors[pres.doctor_id]
        
        if let name_val = sig!.sig_dict["medication"]![0]["pri_name"] as? String
        {
            if let sec_name_val = sig!.sig_dict["medication"]![0]["sec_name"] as? String{
                if sec_name_val != "*&*"{
                    cell.nameLabel.text=name_val + "-" + sec_name_val
                }
                else{
                    cell.nameLabel.text=name_val
                }
            }
            else
            {
                cell.nameLabel.text=name_val
            }
        }
        cell.subNameLabel.text=doctor_name
        return cell
    }

    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
    
        if segue!.identifier=="segueToPrescriptionDetail"{
            let z=segue!.destinationViewController as! PrescriptionDetailTableViewController
            let x=sender as? NSIndexPath
            z.prescription = self.prescriptions[x!.row]
            z.sig=self.sigs[(z.prescription?.prescription_id)!]
        }
        
    }
    
    
    @IBAction func editButtonPressed(sender: AnyObject) {
        if(!areEditing){
            areEditing = true
            editButton.title="Save"
        }
        else if(areEditing)
        {
            var id_arr = [Int]()
            for x in prescriptions{
                id_arr.append(x.prescription_id)
            }
            self.updatePatientPrescription(id_arr)
            
        }
    }
    
    func updatePatientPrescription(arr: [Int])
    {
          RestApiManager.sharedInstance.makeRequest(updatePatientPrescriptionCallBack, prefixType:ReqPrefix.Update, pathType: ReqPath.PatientPrescription, args: ("patient_id",GlobalState.globalState.patient_id), ("prescription_preferences",arr))
    }
    
    
    func updatePatientPrescriptionCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()

    {
        if error==nil
        {
            areEditing=false;
            editButton.title="Edit"
            prescriptions.removeAll()
            self.loadPrescriptions()
        }
    
    }
    
    func longPressGestureRecognized(gestureRecognizer: UIGestureRecognizer) {
        
        if(!areEditing){return }
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.locationInView(tableView)
        let indexPath = tableView.indexPathForRowAtPoint(locationInView)
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        struct Path {
            static var initialIndexPath : NSIndexPath? = nil
        }
        
        switch state {
        case UIGestureRecognizerState.Began:
            if indexPath != nil {
                Path.initialIndexPath = indexPath
                let cell = tableView.cellForRowAtIndexPath(indexPath!) as UITableViewCell!
                My.cellSnapshot  = snapshotOfCell(cell)
                
                var center = cell.center
                My.cellSnapshot!.center = center
                My.cellSnapshot!.alpha = 0.0
                tableView.addSubview(My.cellSnapshot!)
                
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    My.cellIsAnimating = true
                    My.cellSnapshot!.center = center
                    My.cellSnapshot!.transform = CGAffineTransformMakeScale(1.05, 1.05)
                    My.cellSnapshot!.alpha = 0.98
                    cell.alpha = 0.0
                    }, completion: { (finished) -> Void in
                        if finished {
                            My.cellIsAnimating = false
                            if My.cellNeedToShow {
                                My.cellNeedToShow = false
                                UIView.animateWithDuration(0.25, animations: { () -> Void in
                                    cell.alpha = 1
                                })
                            } else {
                                cell.hidden = true
                            }
                        }
                })
            }
            
        case UIGestureRecognizerState.Changed:
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                My.cellSnapshot!.center = center
                
                if ((indexPath != nil) && (indexPath != Path.initialIndexPath)) {
                    //Order has changed
                    prescriptions.insert(prescriptions.removeAtIndex(Path.initialIndexPath!.row), atIndex: indexPath!.row)
                    tableView.moveRowAtIndexPath(Path.initialIndexPath!, toIndexPath: indexPath!)
                    Path.initialIndexPath = indexPath
                    
                    
                }
            }
        default:
            if Path.initialIndexPath != nil {
                let cell = tableView.cellForRowAtIndexPath(Path.initialIndexPath!) as UITableViewCell!
                if My.cellIsAnimating {
                    My.cellNeedToShow = true
                } else {
                    cell.hidden = false
                    cell.alpha = 0.0
                }
                
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    My.cellSnapshot!.center = cell.center
                    My.cellSnapshot!.transform = CGAffineTransformIdentity
                    My.cellSnapshot!.alpha = 0.0
                    cell.alpha = 1.0
                    
                    }, completion: { (finished) -> Void in
                        if finished {
                            Path.initialIndexPath = nil
                            My.cellSnapshot!.removeFromSuperview()
                            My.cellSnapshot = nil
                        }
                })
            }
        }
    }
    
    func snapshotOfCell(inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }

    
    
    
}

