//
//  SigComponent.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/8/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit



class SigComponent
{
    var category : String?
    var title_text : String?
    var value_text : String?
    
    var key_string : String?
    var value_string : String?
    var value_num : Double?
    var missing : Bool
    var value_bool : Bool?
    var type : ValueType
    
    init(title: String, key: String, category: String, type: ValueType)
    {
        self.missing=true;
        self.title_text=title
        self.key_string=key
        self.category=category
        self.type=type
    
    }
    
    init(title: String, value: String, key: String, category: String, type: ValueType)
    {
        self.missing=false;
        self.title_text=title
        self.value_text=value;
        self.key_string=key
        self.category=category
        self.type=type
    }
}
