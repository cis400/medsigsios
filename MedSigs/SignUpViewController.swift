//
//  SignUpViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/29/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class SignUpViewController : UIViewController
{

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    

    @IBOutlet weak var typeSegmentControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.navigationItem.title="Sign Up"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    @IBAction func submitButtonClicked(sender: AnyObject) {
    
        if passwordTextField.text==confirmPasswordTextField.text
        {
            let account_t = typeSegmentControl.selectedSegmentIndex==1 ? "doctor" : "patient"
            RestApiManager.sharedInstance.makeRequest(addUser, prefixType: ReqPrefix.User, pathType: ReqPath.UserCreate, args: ("username",emailTextField.text!),("password",passwordTextField.text!),("type",account_t))
        }
    
    }
    

   
    
    func addUser(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        if error==nil
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                //Login worked
                let id = dict[0]["user_id"] as! Int
                let type = dict[0]["type"]  as! String
                if(type=="doctor")
                {
                    GlobalState.globalState.doctor_id=id
                    GlobalState.globalState.isDoctor=true
                    
                    
                }
                else
                {
                    GlobalState.globalState.patient_id=id
                    GlobalState.globalState.isDoctor=false
                   
                }
                performSegueWithIdentifier("segueToUserData", sender: nil)
                
            }
            else
            {
                print("error in creating the user")
            }
        }
        else
        {
            print("error in API call")
        }
        
    }
    

}
