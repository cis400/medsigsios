//
//  SecondConfirmationTableViewCell.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/5/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class SecondConfirmationTableViewCell : UITableViewCell
{
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var valueLabel: UILabel!
    static var height: CGFloat = 60
}
