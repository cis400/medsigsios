//
//  PrescriptionDetailTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class PrescriptionDetailTableViewController: UITableViewController
{
    var sig : Sig?
    var prescription : Prescription?
    var components = [[SigComponent]]()
    var sections = [(String,String)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title=sig?.sig_dict["medication"]!["pri_name"] as? String
        tableView.rowHeight=PrescriptionDetailTableViewCell.height
        loadProperties()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadProperties()
    {
        let (sections, components) = (sig?.get_table_info())!
        self.sections.appendContentsOf(sections)
        self.components.appendContentsOf(components)
        print(sections)
        print(components)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier=="segueToPrescriptionTimePrefs"{
        
            let x = segue.destinationViewController as! SettingsTableViewController
            x.pres_id=(prescription?.prescription_id)!
            x.isIndividual=true
        }
    
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return components[section].count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "PrescriptionDetailTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PrescriptionDetailTableViewCell
        cell.nameLabel.text=components[indexPath.section][indexPath.row].title_text
        cell.valueLabel.text=components[indexPath.section][indexPath.row].value_text
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section].0
    }
    
    @IBAction func unwindToPatientDetail(segue: UIStoryboardSegue){
    
    }

}
