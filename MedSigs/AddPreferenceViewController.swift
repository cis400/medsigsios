//
//  AddPreferenceViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/4/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class AddPreferenceViewController : UIViewController
{
    
    var date : NSDate?
    var timeId=0;
    var old : TimePreference?
    @IBOutlet weak var trashButton: UIBarButtonItem!
    
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var foodSegmentControl: UISegmentedControl!
    
    func editInit(old :TimePreference)
    {
        self.timeId=old.time_id
        self.navigationItem.title="Editing Preference"
        self.date=old.time
        self.old = old;
    }
    
    @IBAction func trashButtonPressed(sender: AnyObject) {
        performSegueWithIdentifier("unwindToDeletePreference", sender: sender)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(timeId==0)
        {
            self.date = datePicker.date
            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.datePicker.setDate(formatter.dateFromString("1970-01-01 12:00:00")!, animated : false)
        }
        else
        {
            
            datePicker.setDate(self.date!, animated: true)
            
            if old!.food{
                self.foodSegmentControl.selectedSegmentIndex=0
            }
            else{
                self.foodSegmentControl.selectedSegmentIndex=1
            }
            self.descriptionTextField.text=old!.description
        }
        
    }
    
    @IBAction func dateChanged(sender: AnyObject) {
        self.date = datePicker.date
        print(self.date)
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "unwindToSavePreference"{
            
            var var_dict = [String: AnyObject]()
            
            var_dict["description"] = descriptionTextField.text
            var_dict["time"]=DateTimeManager.instance.convertToServerString(datePicker.date)
            var_dict["time_id"] = self.timeId
            var_dict["food"] = (foodSegmentControl.selectedSegmentIndex==0)
            if(timeId==0)
            {
                var_dict["wake"]=false
                var_dict["sleep"]=false
            }
            else{
                var_dict["wake"]=old?.wake
                var_dict["sleep"]=old?.sleep
            }
           
            
            let pref = TimePreference(dic: var_dict)
            
            let x = segue.destinationViewController as! SettingsTableViewController
            x.newPref = pref
        }
        else if (segue.identifier=="unwindToDeletePreference")
        {
            let x = segue.destinationViewController as! SettingsTableViewController
            x.delete_id = self.timeId
            
        }
    }
    
    
}
