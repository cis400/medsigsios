//
//  HomeTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/9/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class HomeTableViewController: UITableViewController{
    //@IBOutlet weak var menuButton:UIBarButtonItem!
    
    var events = [Event]()
    var events_manager = EventsManager()
    var front_schedule = [(NSDate, String, [Event])]()
    var fake_time_strings = ["7:00 am", "12:00 pm", "9:00 pm"]
    var fake_prescriptions = ["Oxycodone/Aspirin\nCrestor\nAdvair","Oxycodone/Aspirin","Oxycodone/Aspirin\nAdvair"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //let menuButton=self.navigationItem.leftBarButtonItem
        
        if self.revealViewController() != nil {
            
            self.navigationItem.leftBarButtonItem!.target = self.revealViewController()
            self.navigationItem.leftBarButtonItem!.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        loadEvents()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadEvents()
    {
        let beginDate = NSCalendar.currentCalendar().dateBySettingHour(0, minute: 0, second: 0, ofDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
        
        let endDate = NSCalendar.currentCalendar().dateBySettingHour(23, minute: 59, second: 0, ofDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
        print(beginDate)
        print(endDate)
        
        print(DateTimeManager.instance.convertToServerStringWhole(beginDate!))
        print( DateTimeManager.instance.convertToServerStringWhole(endDate!))
        
        
        
        RestApiManager.sharedInstance.makeRequest(addEvents, prefixType: ReqPrefix.Get, pathType: ReqPath.PatientSchedule, args: ("patient_id",GlobalState.globalState.patient_id), ("begin_date", DateTimeManager.instance.convertToServerStringWhole(beginDate!)), ("end_date", DateTimeManager.instance.convertToServerStringWhole(endDate!)))
        
    }
    
    func addEvents(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        // use responseObject and error here
        print("responseObject = \(responseObject); error = \(error)")
        if error==nil
        {
            for x in (responseObject?.objectForKey("sol")) as! NSArray{
                self.events.append(Event(arr: x))
            }
            self.events_manager.addEvents(self.events)
            self.front_schedule.appendContentsOf(self.events_manager.getFrontSchedule())
            print(front_schedule)
        }
        else
        {
            print("API call error")
        }
        tableView.reloadData()
        
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return front_schedule.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "ScheduleTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ScheduleTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        
        let (date, names, events) = front_schedule[indexPath.row]
        print((date, names, events))
       
        cell.subNameLabel.text=names
        cell.timeLabel.text = DateTimeManager.instance.getTimeOnlyString(date)
        cell.events.appendContentsOf(events)

        
        return cell
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("segueToScheduleDetail", sender: indexPath)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier=="segueToScheduleDetail"
        {
            let nav = segue.destinationViewController as! UINavigationController
            let dest = nav.viewControllers[0] as! ScheduleDetailTableViewController
            let index = sender as! NSIndexPath
            dest.start_date = self.front_schedule[index.row].0
            dest.events_manager = self.events_manager
        }
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    
}
