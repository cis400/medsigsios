//
//  LoginViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/27/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController
{
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.attributedPlaceholder = NSAttributedString(string:" Email",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        passwordTextField.attributedPlaceholder = NSAttributedString(string:" Password",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
    }

    @IBAction func loginButtonPressed(sender: AnyObject) {
        performLogin(emailTextField.text!, password: passwordTextField.text!);
    }
    
    func performLogin(username: String, password: String){
          RestApiManager.sharedInstance.makeRequest(loginResult, prefixType: ReqPrefix.User, pathType: ReqPath.UserLogin, args: ("username",username), ("password", password))
        
    }
    
    func getDoctorData(doctor_id : Int)
    {
        RestApiManager.sharedInstance.makeRequest(getDoctorDataCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.DoctorData, args: ("doctor_id",doctor_id))
    }
    
    func getDoctorDataCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if error==nil{
            let dict = responseObject?.objectForKey("sol") as! NSArray
            GlobalState.globalState.name=dict[0]["doctor_name"] as! String
            performSegueWithIdentifier("loginDoctor", sender: nil)
        
        }
    }
    
    func getPatientData(patient_id: Int)
    {
        RestApiManager.sharedInstance.makeRequest(getPatientDataCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.PatientData, args: ("patient_id",patient_id))
        
    }
    
    func getPatientDataCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if error==nil{
            let dict = responseObject?.objectForKey("sol") as! NSArray
            GlobalState.globalState.name=dict[0]["patient_name"] as! String
            performSegueWithIdentifier("loginPatient", sender: nil)
        }
    }
    
    func loginResult(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if error==nil
        {
            print(responseObject)
                let dict = responseObject?.objectForKey("sol") as! NSArray
                if(dict.count==1){
                    //Login worked
                    let id = dict[0]["user_id"] as! Int
                    let type = dict[0]["type"]  as! String
                    if(type=="doctor")
                    {
                        GlobalState.globalState.doctor_id=id
                        GlobalState.globalState.isDoctor=true
                        getDoctorData(id)
                    }
                    else
                    {
                        GlobalState.globalState.patient_id=id
                        GlobalState.globalState.isDoctor=false
                        getPatientData(id)
                        
                    }
                }
                else
                {
                    //Login failed
                }
        }
        else
        {
            //Error in API Call
        
        }
    
    
    }

    @IBAction func signupButtonPressed(sender: AnyObject) {
    
    }
    
    @IBAction func unwindFromSignUp(segue: UIStoryboardSegue)
    {
    
    }
}
