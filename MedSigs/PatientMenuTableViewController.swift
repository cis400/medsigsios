//
//  PatientMenuTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/5/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class PatientMenuTableViewController : UITableViewController{


    @IBOutlet weak var patientNameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        patientNameLabel.text=GlobalState.globalState.name
        self.tableView.tableFooterView = UIView()
    }


}
