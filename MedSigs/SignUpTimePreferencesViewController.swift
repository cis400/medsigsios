//
//  SignUpTimePreferencesViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/8/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class SingUpTimePreferencesViewConroller : UIViewController
{
    var curIndex=0;
    var maxIndex=4;
    var curDate : NSDate?
    
    var curLabelText = ["When do you wakeup?", "When do you eat breakfast?", "When do you eat lunch?", "When do you eat dinner?", "When do go to bed?"]
    var curDescription = ["Wakeup","Breakfast","Lunch","Dinner","Bed"]
    var curFood = [false, true, true, true, false]
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.datePicker.setDate(DateTimeManager.instance.convertFromServerString("2016-01-07 12:00:00"), animated: true)
        self.curDate=datePicker.date
        reloadView()
    }
    
    func addTimePreference()
    {
        curDate = self.datePicker.date
        RestApiManager.sharedInstance.makeRequest(addTimePreferenceCallBack, prefixType:ReqPrefix.Insert, pathType: ReqPath.TimeData, args: ("patient_id",GlobalState.globalState.patient_id),
            ("time", DateTimeManager.instance.convertToServerString((curDate)!)),
            ("description", curDescription[curIndex]),
            ("food", curFood[curIndex] ),
            ("wake",  (curIndex==0) ),
            ("sleep", (curIndex==maxIndex) )
        )
    }
    
    func addTimePreferenceCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
    
        
        if error==nil
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                //Successfully added the time 
                if (curIndex<maxIndex)
                {
                    curIndex++
                    reloadView()
                }
                else
                {
                    curIndex++
                    datePicker.hidden=true;
                    titleLabel.text="You can add more preferences in Settings!"
                    button.sizeToFit()
                    button.titleLabel?.text="Finsh"
                }
                
                
                
            }
        }
    }
    
    @IBAction func buttonPressed(sender: AnyObject)
    {
        if(curIndex>maxIndex)
        {
            performSegueWithIdentifier("segueToPatientHome", sender: self)
        }
        else
        {
            addTimePreference()
        }
        
    }
    
    
    func reloadView()
    {
        self.titleLabel.text=curLabelText[curIndex]
        self.datePicker.setDate(DateTimeManager.instance.convertFromServerString("2016-01-07 12:00:00"), animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func datePickerChanged(sender: AnyObject)
    {
        self.curDate=datePicker.date
    }

    
}
