//
//  PatientsTableViewCell.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class PatientsTableViewCell: UITableViewCell
{
    @IBOutlet weak var nameLabel: UILabel!
    var patient : Patient?
}
