//
//  UserDataTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/3/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class UserDataTableViewController : UITableViewController
{


    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    


    @IBAction func saveButtonClicked(sender: AnyObject) {
        
        
        if GlobalState.globalState.isDoctor{
            RestApiManager.sharedInstance.makeRequest(updateDataCallBack, prefixType: ReqPrefix.Update, pathType: ReqPath.DoctorData, args: ("doctor_id", GlobalState.globalState.doctor_id), ("doctor_name",nameTextField.text!))
        }
        else{
            RestApiManager.sharedInstance.makeRequest(updateDataCallBack, prefixType: ReqPrefix.Update, pathType: ReqPath.PatientData, args: ("patient_id", GlobalState.globalState.patient_id), ("patient_name",nameTextField.text!))
            print("Adding the patient name\n")
            print(nameTextField.text)
        }
        
        
        
    }
    
    
    func updateDataCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        if error==nil
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                if GlobalState.globalState.isDoctor{
                    performSegueWithIdentifier("segueToDoctorHome", sender: nil)
                }
                else{
                    performSegueWithIdentifier("segueToPatientHome", sender: nil)
                }
    
            }
            else
            {
                print("error in creating the user")
            }
        }
        else
        {
            print("error in API call")
        }
        
    }

}
