//
//  SignUpNavigationController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit


class SignUpNavigationController : UINavigationController
{


    override func viewDidLoad() {
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.barTintColor = UIColor(netHex: 0x95022B)
        self.navigationBar.titleTextAttributes=[NSForegroundColorAttributeName : UIColor.whiteColor()]
    }


}
