//
//  Prescription.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/12/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class Prescription{
    //MARK: Properties
    
    var prescription_id : Int
    var doctor_id : Int
    var last_taken : NSDate?
    
    //MARK: Initialization
    init(arr: AnyObject){
        prescription_id=arr["prescription_id"] as! Int
        doctor_id=arr["doctor_id"] as! Int
        if  let last_taken_str = arr["last_taken"] as? String{
            last_taken=DateTimeManager.instance.convertFromServerString(last_taken_str)
        }
        
    }


}
