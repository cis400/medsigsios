//
//  Events.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit


class Event{
    var start_time: NSDate
    var end_time: NSDate
    var med_name:String
    var instruction: String
    var prescription_id: Int
   
    init(arr: AnyObject)
    {
        self.start_time=DateTimeManager.instance.convertFromServerString( (arr["start_time"] as! String))
        self.end_time=DateTimeManager.instance.convertFromServerString( (arr["end_time"] as! String))
        self.prescription_id=arr["prescription_id"] as! Int
        self.instruction=arr["instruction"] as! String
        self.med_name=arr["med_name"] as! String
    }
    
    
}
