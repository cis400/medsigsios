//
//  SettingsTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/9/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController{
    //@IBOutlet weak var menuButton:UIBarButtonItem!
    
    var itemsArray = [TimePreference]()
    var newPref: TimePreference?
    var areEditing: Bool = false
    var isIndividual = false
    var pres_id=0
    var delete_id : Int?
    var first : Bool = true
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.areEditing = false;
        
        loadTimes()
       
        
        if (self.revealViewController() != nil && !self.isIndividual)
        {
            self.setEditing(false, animated: false)
        }
        else{
            self.setEditing(false, animated: false)
        }
        navigationItem.rightBarButtonItem = editButtonItem()
        let longpress = UILongPressGestureRecognizer(target: self, action: "longPressGestureRecognized:")
        tableView.addGestureRecognizer(longpress)
        self.tableView.tableFooterView = UIView()
        first = false;
    }
  
    func backButtonPressed(sender: UIButton){
        performSegueWithIdentifier("unwindToPatientDetail", sender: sender)
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        self.areEditing=editing
      
        if(editing)
        {
            self.view.removeGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "addButtonPressed:")
            
            // Toggles the edit button state
            super.setEditing(editing, animated: animated)
            // Toggles the actual editing actions appearing on a table view
            tableView.setEditing(editing, animated: true)
        }
        else
        {
            var arr = [Int]()
            for x in self.itemsArray{
                arr.append(x.time_id)
            }
            if(!first){
                self.updatePatientPreference(arr)
            }
            
            super.setEditing(editing, animated: animated)
            // Toggles the actual editing actions appearing on a table view
            tableView.setEditing(editing, animated: true)
            
            if(self.isIndividual){
                self.navigationItem.leftBarButtonItems?.removeFirst()
                self.navigationItem.setHidesBackButton(false, animated: false)
            }else{
                self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menu"), style: .Plain, target: self.revealViewController(), action: "revealToggle:")
            }
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    func addButtonPressed(sender : UIBarButtonItem)
    {
        
        self.setEditing(false, animated: true)
        performSegueWithIdentifier("segueToAddTime", sender: self)
    }
    
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            
            let id = itemsArray[indexPath.row].time_id
            self.deleteHandler(id)
            
            itemsArray.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func deleteHandler(delete_id : Int)
    {
        if(self.isIndividual)
        {
            RestApiManager.sharedInstance.makeRequest(deleteTimeCallBack, prefixType: ReqPrefix.Remove, pathType: ReqPath.TimeData, args: ("time_id",delete_id), ("prescription_id", self.pres_id))
        }
        else
        {
        RestApiManager.sharedInstance.makeRequest(deleteTimeCallBack, prefixType: ReqPrefix.Remove, pathType: ReqPath.TimeData , args: ("time_id",delete_id))
        }
    }
    
    func deleteTimeCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if(error==nil)
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                
            }
            else
            {
                //Adding the prescription failed
            }
            
        }
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if self.itemsArray[indexPath.row].wake || self.itemsArray[indexPath.row].sleep
        {
            false
        }
        return true
    }
    
    func loadTimes()
    {
        if(self.isIndividual){
              RestApiManager.sharedInstance.makeRequest(loadTimesCallBack, prefixType:ReqPrefix.Get, pathType: ReqPath.PrescriptionPreference, args: ("prescription_id",self.pres_id))
        }else{
         RestApiManager.sharedInstance.makeRequest(loadTimesCallBack, prefixType:ReqPrefix.Get, pathType: ReqPath.PatientPreference, args: ("patient_id",GlobalState.globalState.patient_id))
        }
    }
    
    func loadTimesCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        print(responseObject)
        
        if(error==nil){
            itemsArray.removeAll()
            for x in (responseObject?.objectForKey("sol")) as! NSArray{
                
                self.itemsArray.append(TimePreference(dic: x))
            }
        }
        tableView.reloadData()
    }
    
    @IBAction func unwindDeletePreference(segue: UIStoryboardSegue){
        /*RestApiManager.sharedInstance.makeRequest(loadTimesCallBack, prefixType:ReqPrefix.Remove, pathType: ReqPath., args: ("patient_id",GlobalState.globalState.patient_id))*/
        
    }

    func deletePreferenceCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if error==nil
        {
            self.itemsArray.removeAll()
            self.loadTimes()
        }
    
    }
    
    @IBAction func unwindToSavePreference(segue: UIStoryboardSegue) {
        
        if(self.newPref?.time_id==0)
        {
            if(!self.isIndividual){
            RestApiManager.sharedInstance.makeRequest(addTimeCallBack, prefixType:ReqPrefix.Insert, pathType: ReqPath.TimeData, args: ("patient_id",GlobalState.globalState.patient_id),
                ("time", DateTimeManager.instance.convertToServerString((newPref?.time)!)),
                ("description", (self.newPref?.description)!),
                ("food", (self.newPref?.food)!),
                ("wake", (self.newPref?.wake)!),
                ("sleep",(self.newPref?.sleep)!))
        }
            else{
                RestApiManager.sharedInstance.makeRequest(addTimeCallBack, prefixType:ReqPrefix.Insert, pathType: ReqPath.TimeData, args: ("patient_id",GlobalState.globalState.patient_id),
                    ("time", DateTimeManager.instance.convertToServerString((newPref?.time)!)),
                     ("prescription_id",self.pres_id),
                    ("description", (self.newPref?.description)!),
                    ("food", (self.newPref?.food)!),
                    ("wake", (self.newPref?.wake)!),
                    ("sleep",(self.newPref?.sleep)!))
            }
        }
        else{
            if(!self.isIndividual){
            RestApiManager.sharedInstance.makeRequest(addTimeCallBack, prefixType:ReqPrefix.Update, pathType: ReqPath.TimeData, args: ("patient_id",GlobalState.globalState.patient_id),
                ("time", DateTimeManager.instance.convertToServerString((newPref?.time)!)),
                ("description", (self.newPref?.description)!),
                ("food", (self.newPref?.food)!),
                 ("time_id", (newPref?.time_id)!),
                ("wake", (self.newPref?.wake)!),
                ("sleep",(self.newPref?.sleep)!))
            }
            else{
                RestApiManager.sharedInstance.makeRequest(addTimeCallBack, prefixType:ReqPrefix.Update, pathType: ReqPath.TimeData, args: ("patient_id",GlobalState.globalState.patient_id),
                    ("time", DateTimeManager.instance.convertToServerString((newPref?.time)!)),
                    ("time_id", (newPref?.time_id)!),
                    ("description", (self.newPref?.description)!),
                    ("food", (self.newPref?.food)!),
                    ("wake", (self.newPref?.wake)!),
                    ("sleep",(self.newPref?.sleep)!))
            
            }
            }
    }
    
    
    func addTimeCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if error==nil
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                itemsArray.removeAll()
                self.loadTimes()
            }
        }
        
    }
    
    
    func updatePatientPreference(arr: [Int])
    {
        if(self.isIndividual)
        {
           RestApiManager.sharedInstance.makeRequest(updatePatientPreferenceCallBack, prefixType:ReqPrefix.Update, pathType: ReqPath.PrescriptionPreference, args: ("prescription_id",self.pres_id), ("time_preferences",arr), ("patient_id", GlobalState.globalState.patient_id))
        }
        else
        {
        RestApiManager.sharedInstance.makeRequest(updatePatientPreferenceCallBack, prefixType:ReqPrefix.Update, pathType: ReqPath.PatientPreference, args: ("patient_id",GlobalState.globalState.patient_id), ("time_preferences",arr))
        }
    }
    
    
    
    func updatePatientPreferenceCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if error==nil
        {
            areEditing=false;
            itemsArray.removeAll()
            self.loadTimes()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       
        if segue.identifier=="segueToEditTime"{
            let x = sender as? TimePreference
            let dest_nav = segue.destinationViewController as? UINavigationController
            let dest_view = dest_nav?.viewControllers[0] as! AddPreferenceViewController
            dest_view.editInit(x!)
            dest_view.timeId=(x?.time_id)!
            print("Accessory selected")
        }
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if(!areEditing)
        {
            performSegueWithIdentifier("segueToEditTime", sender: self.itemsArray[indexPath.row])
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "SettingsTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! SettingsTableViewCell
        
        let t = itemsArray[indexPath.row]
        
        cell.nameLabel.text=t.description
        cell.timeLabel.text=DateTimeManager.instance.getTimeOnlyString(t.time)
        if(areEditing){
            cell.accessoryType = .None
        }
        else
        {
            cell.accessoryType = .DisclosureIndicator
        }
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func longPressGestureRecognized(gestureRecognizer: UIGestureRecognizer) {
        
        if(!areEditing){return }
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.locationInView(tableView)
        let indexPath = tableView.indexPathForRowAtPoint(locationInView)
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        struct Path {
            static var initialIndexPath : NSIndexPath? = nil
        }
        
        switch state {
        case UIGestureRecognizerState.Began:
            if indexPath != nil {
                Path.initialIndexPath = indexPath
                let cell = tableView.cellForRowAtIndexPath(indexPath!) as UITableViewCell!
                My.cellSnapshot  = snapshotOfCell(cell)
                
                var center = cell.center
                My.cellSnapshot!.center = center
                My.cellSnapshot!.alpha = 0.0
                tableView.addSubview(My.cellSnapshot!)
                
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    My.cellIsAnimating = true
                    My.cellSnapshot!.center = center
                    My.cellSnapshot!.transform = CGAffineTransformMakeScale(1.05, 1.05)
                    My.cellSnapshot!.alpha = 0.98
                    cell.alpha = 0.0
                    }, completion: { (finished) -> Void in
                        if finished {
                            My.cellIsAnimating = false
                            if My.cellNeedToShow {
                                My.cellNeedToShow = false
                                UIView.animateWithDuration(0.25, animations: { () -> Void in
                                    cell.alpha = 1
                                })
                            } else {
                                cell.hidden = true
                            }
                        }
                })
            }
            
        case UIGestureRecognizerState.Changed:
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                My.cellSnapshot!.center = center
                
                if ((indexPath != nil) && (indexPath != Path.initialIndexPath)) {
                    //Order has changed
                    itemsArray.insert(itemsArray.removeAtIndex(Path.initialIndexPath!.row), atIndex: indexPath!.row)
                    tableView.moveRowAtIndexPath(Path.initialIndexPath!, toIndexPath: indexPath!)
                    Path.initialIndexPath = indexPath
                    
                    
                }
            }
        default:
            if Path.initialIndexPath != nil {
                let cell = tableView.cellForRowAtIndexPath(Path.initialIndexPath!) as UITableViewCell!
                if My.cellIsAnimating {
                    My.cellNeedToShow = true
                } else {
                    cell.hidden = false
                    cell.alpha = 0.0
                }
                
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    My.cellSnapshot!.center = cell.center
                    My.cellSnapshot!.transform = CGAffineTransformIdentity
                    My.cellSnapshot!.alpha = 0.0
                    cell.alpha = 1.0
                    
                    }, completion: { (finished) -> Void in
                        if finished {
                            Path.initialIndexPath = nil
                            My.cellSnapshot!.removeFromSuperview()
                            My.cellSnapshot = nil
                        }
                })
            }
        }
    }
    
    func snapshotOfCell(inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
 
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
