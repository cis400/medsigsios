//
//  ScheduleTableViewCell.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class ScheduleTableViewCell : UITableViewCell
{
    @IBOutlet weak var subNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    static var height: CGFloat = 70
    var events = [Event]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }


}
