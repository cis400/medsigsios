//
//  PatientDetailTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class PatientDetailTableViewController : UITableViewController{
    var pat: Patient?
    var sigs = [Sig]()
    var new_added=false
    var new_sig : Sig?
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "PrescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "PrescriptionTableViewCell")
        tableView.rowHeight=PrescriptionsTableViewCell.height
        self.loadPrescriptions()
        self.navigationItem.title=pat?.patient_name
        self.tableView.tableFooterView = UIView()
        
        navigationItem.rightBarButtonItem = editButtonItem()
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        if(editing)
        {
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "addButtonPressed:")
            self.navigationItem.setHidesBackButton(true, animated: true)
            // Toggles the edit button state
            super.setEditing(editing, animated: animated)
            // Toggles the actual editing actions appearing on a table view
            tableView.setEditing(editing, animated: true)
        }
        else
        {
            super.setEditing(editing, animated: animated)
            // Toggles the actual editing actions appearing on a table view
            tableView.setEditing(editing, animated: true)
            self.navigationItem.setHidesBackButton(false, animated: true)
            self.navigationItem.leftBarButtonItems?.removeFirst()
        }
    }
    
    func addButtonPressed(sender : UIBarButtonItem)
    {
        
        self.setEditing(false, animated: true)
        performSegueWithIdentifier("AddPrescription", sender: self)
    }
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            print(sigs)
            let id = sigs[indexPath.row].sig_dict["prescription_id"] as! Int
            self.deleteHandler(id)
            
            sigs.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! PrescriptionsTableViewCell
        
        performSegueWithIdentifier("segueToEditPrescription", sender: cell)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier=="AddPrescription"{
            let dest_nav = segue.destinationViewController as! UINavigationController
            let dest=dest_nav.viewControllers[0] as! NewPrescriptionController
            print("Preparing the Add prescription segue!!!!!!!")
            dest.patient=self.pat
            
        }
        if segue.identifier=="segueToEditPrescription"
        {
            let dest_nav = segue.destinationViewController as! UINavigationController
            let dest=dest_nav.viewControllers[0] as! ConfirmationTableViewController
            let cell = sender as! PrescriptionsTableViewCell
            dest.isEdit = true
            dest.sig=cell.sig
        }
    }
    
    func saveHandler()
    {
        let x = self.new_sig?.formattedDict()
        print(x)
        RestApiManager.sharedInstance.makeRequest(insertPrescriptionCallBack, prefixType: ReqPrefix.Insert, pathType: ReqPath.PrescriptionData, args: x!)
    }
    
    func updateHandler()
    {
        let x = self.new_sig?.formattedDict()
        print(x)
        RestApiManager.sharedInstance.makeRequest(insertPrescriptionCallBack, prefixType: ReqPrefix.Update, pathType: ReqPath.PrescriptionData, args: x!)
    }
    
    func deleteHandler(delete_id : Int)
    {
        RestApiManager.sharedInstance.makeRequest(deletePrescriptionCallBack, prefixType: ReqPrefix.Remove, pathType: ReqPath.PrescriptionData, args: ("prescription_id",delete_id))
    }
    
    func deletePrescriptionCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if(error==nil)
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                
            }
            else
            {
                //Adding the prescription failed
            }
            
        }
        
    }
    
    func insertPrescriptionCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if(error==nil)
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                self.sigs.removeAll()
                self.loadPrescriptions()
            }
            else
            {
                //Adding the prescription failed
            }

        }
    
    }
    
    func loadPrescriptions(){
        RestApiManager.sharedInstance.makeRequest(loadPrescriptionsCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.PatientPrescription, args: ("doctor_id",GlobalState.globalState.doctor_id), ("patient_id", (pat?.patient_id)!))
    }
    
    func loadPrescriptionsCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        // use responseObject and error here
        //print("responseObject = \(responseObject); error = \(error)")
        var prescription_ids = [Int]()
        if(error==nil){
            for x in (responseObject?.objectForKey("sol")) as! NSArray{
                prescription_ids.append(x["prescription_id"] as! Int)
            }
        }
        self.loadSigs(prescription_ids)
        tableView.reloadData()
    }

    func loadSigs(prescription_ids : [Int])
    {
        for x in prescription_ids{
         RestApiManager.sharedInstance.makeRequest(loadSigsCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.PrescriptionData, args: ("prescription_id",x ))
            
        }
    }
    
    func loadSigsCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        print(responseObject)
        if error==nil
        {
            let temp_sig = Sig(dic : (responseObject!["sol"]! as! [NSDictionary])[0])
            temp_sig.detail_init((responseObject!["sol"] as! [NSDictionary])[0])
            temp_sig.sig_dict["prescription_id"] = parameters["prescription_id"] as! Int
            self.sigs.append(temp_sig)
            
        }
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sigs.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "PrescriptionTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PrescriptionsTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        let sig = sigs[indexPath.row]
        
        if let name_val = sig.sig_dict["medication"]![0]["pri_name"] as? String
        {
            if let sec_name_val = sig.sig_dict["medication"]![0]["sec_name"] as? String{
                if sec_name_val != "*&*"{
                    cell.nameLabel.text=name_val + "-" + sec_name_val
                }
                else{
                cell.nameLabel.text=name_val
                }
            }
            else
            {
                cell.nameLabel.text=name_val
            }
        }
        else
        {
            cell.nameLabel.text="No Name Given"
        }
        cell.subNameLabel.text=""//"Doctor Name"
        cell.sig = sig
        return cell
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func unwindToUpdate(segue: UIStoryboardSegue){
        self.updateHandler()
    }
    
    @IBAction func unwindToFirstViewController(segue: UIStoryboardSegue) {
        if(segue.identifier=="unwindToPatientDetail" )
        {
            self.saveHandler()
        }
    }

    
}
