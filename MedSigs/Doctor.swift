//
//  Doctor.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class Doctor{

    //MARK: Properties
    var id: Int //Primary Key
    var name: String
    
    //MARK: Initialization
    init(arr: AnyObject){
        self.id=arr["doctor_id"] as! Int
        self.name=arr["doctor_name"] as! String
    }

}
