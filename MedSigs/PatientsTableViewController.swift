//
//  PatientsTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//


import UIKit

class PatientsTableViewController: UITableViewController
{
    var patients=[Patient]()
    var detail : Patient?
    var new_patient : String = ""
    
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.loadPatients()
        
        if self.revealViewController() != nil {
            self.setEditing(false, animated: false)
        }
        navigationItem.rightBarButtonItem = editButtonItem()
        tableView.rowHeight=90
        self.tableView.tableFooterView = UIView()
    }
 
    
    override func setEditing(editing: Bool, animated: Bool) {
        
        self.navigationItem.leftBarButtonItems?.removeFirst()
        if(editing)
        {
            self.view.removeGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "addButtonPressed:")
            
            // Toggles the edit button state
            super.setEditing(editing, animated: animated)
            // Toggles the actual editing actions appearing on a table view
            tableView.setEditing(editing, animated: true)
        }
        else
        {
            super.setEditing(editing, animated: animated)
            // Toggles the actual editing actions appearing on a table view
            tableView.setEditing(editing, animated: true)
            
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menu"), style: .Plain, target: self.revealViewController(), action: "revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    func addButtonPressed(sender : UIBarButtonItem)
    {
        
        self.setEditing(false, animated: true)
        performSegueWithIdentifier("AddPatient", sender: self)
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
        
            let id = patients[indexPath.row].patient_id
            self.deleteHandler(id)
            
            patients.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func deleteHandler(delete_id : Int)
    {
        RestApiManager.sharedInstance.makeRequest(deletePatientCallBack, prefixType: ReqPrefix.Remove, pathType: ReqPath.DoctorPatient, args: ("patient_id",delete_id), ("doctor_id", GlobalState.globalState.doctor_id))
    }
    
    
    func deletePatientCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if(error==nil)
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                
            }
            else
            {
                //Adding the prescription failed
            }
            
        }
        
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadPatients()
    {
        RestApiManager.sharedInstance.makeRequest(loadPatientsCallback, prefixType: ReqPrefix.Get, pathType: ReqPath.DoctorPatient, args: ("doctor_id",GlobalState.globalState.doctor_id))
    }
    
    func loadPatientsCallback(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        // use responseObject and error here
        print("responseObject = \(responseObject); error = \(error)")
        if(error==nil){
            for x in (responseObject?.objectForKey("sol")) as! NSArray{
                self.patients.append(Patient(arr: x))
                }
        }
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patients.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "PatientsTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PatientsTableViewCell
        
        
        let pat = patients[indexPath.row]
        cell.patient=pat
        cell.nameLabel.text=pat.patient_name
        return cell
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    //Adding a new Patient
    @IBAction func unwindToPatientViewController(segue: UIStoryboardSegue) {
        lookupPatientId(self.new_patient)
       
    }
    
    func lookupPatientId(email:String)
    {
            RestApiManager.sharedInstance.makeRequest(lookupPatientIdCallback, prefixType: ReqPrefix.Get, pathType: ReqPath.UserData, args: ("username",email))
    }
    
    func lookupPatientIdCallback(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if(error==nil){
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                let id = dict[0]["user_id"] as! Int
                addNewPatient(id)
            }
        }
    }
    
    func addNewPatient(patient_id:Int)
    {
        RestApiManager.sharedInstance.makeRequest(addNewPatientCallback, prefixType: ReqPrefix.Insert, pathType: ReqPath.DoctorPatient, args: ("doctor_id",GlobalState.globalState.doctor_id), ("patient_id", patient_id))
    }
    
    func addNewPatientCallback(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
         print("responseObject = \(responseObject); error = \(error)")
        if(error==nil){
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                patients.removeAll()
                self.loadPatients()
            }
        }
    }
    
    
    //Segue Prep
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PatientDetail" {
            let x = sender as! PatientsTableViewCell
            let z=segue.destinationViewController as! PatientDetailTableViewController
            z.pat=x.patient
        }
        else if segue.identifier=="AddPatient"{
        
        }
        
        
    }
    
}
