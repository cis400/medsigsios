//
//  SigTableViewCell.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class SigTableViewCell : UITableViewCell
{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueTextField: ConfirmationUITextField!
    
    
    @IBOutlet weak var boolSwitch: ConfirmationUISwitch!
   
    
    @IBOutlet weak var valueLabel: UILabel!
    var isNewButton = false;
    static var height: CGFloat = 45
    var sig_comp : SigComponent?

}
