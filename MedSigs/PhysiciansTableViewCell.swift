//
//  PhysiciansTableViewCell.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class PhysiciansTableViewCell: UITableViewCell {
    //MARK: Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
