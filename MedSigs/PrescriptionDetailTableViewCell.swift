//
//  PrescriptionDetailTableViewCell.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import Foundation

class PrescriptionDetailTableViewCell : UITableViewCell
{
    @IBOutlet weak var valueLabel: UILabel!
    
    
    @IBOutlet weak var nameLabel: UILabel!
   
    static var height: CGFloat = 60
    
    
}