//
//  Patient.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class Patient{
    //MARK: Properties
    var patient_id : Int //Primary Key
    var patient_name: String
    
    init(arr: AnyObject)
    {
        self.patient_id = arr["patient_id"] as! Int
        self.patient_name = arr["patient_name"] as! String
    }
}
