//
//  Sig.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit


enum ValueType
{
    case String
    case Number
    case Bool
}

class Sig{
    
    var sig_dict = [String:AnyObject]()
    var type_dict = [(String) : ValueType]()
    var categories = ["dose", "duration", "event", "frequency", "medication", "method", "span"]
    func modify_bottom(key1 :String, key2: String, val: AnyObject)
    {
        var dic = (self.sig_dict[key1]! as! [[String : AnyObject]])[0]
        dic[key2] = val
        var list  = [[String:AnyObject]]()
        list.append(dic)
        self.sig_dict[key1] = list
    }
    
    
    
    func get_table_info() -> ([(String,String)], [[SigComponent]])
    {
        var i = 0
        var k = 0
        var sections = [(String,String)]()
        var components = [[SigComponent]]()
        for x in self.categories{
            if let x_p = self.sig_dict[x]{
                let y = x_p as! [[String:AnyObject]]
                if !y.isEmpty
                {
                    sections.append((x,x))
                    components.append([SigComponent]())
                    for a in y{
                        k=0
                        for (key,value) in a
                        {
                            if !(value is NSNull){
                                let type = self.type_dict[x+key]
                                if(type == ValueType.Number)
                                {
                                    if value.doubleValue>=0
                                    {
                                        
                                        components[i].append(SigComponent(title: key , value: value.stringValue, key: key, category: x, type: ValueType.Number))
                                        components[i].last?.value_num=value.doubleValue
                                        k++
                                    }
                                }
                                else if (type == ValueType.Bool)
                                {
                                    print("*******Found bool")
                                    let val = (value as! String)=="True"
                                    components[i].append(SigComponent(title: key , value: val.description, key: key, category: x, type: ValueType.Bool))
                                    components[i].last?.value_bool = val
                                    k++
                                }
                                else{
                                    if (value as? String) != "*&*"
                                    {
                                        if key == "begin_date"
                                        {
                                            let date_string = (value as! String).substringToIndex((value as! String).startIndex.advancedBy(11))
                                            components[i].append(SigComponent(title: key, value: date_string, key: key, category: x, type: ValueType.String))
                                            components[i].last?.value_string=date_string
                                        }
                                        else{
                                            components[i].append(SigComponent(title: key, value: value as! String , key: key, category: x, type: ValueType.String))
                                            components[i].last?.value_string=value as? String
                                        }
                                        k++
                                    }
                                }
                            }
                        }
                    }
                    if k == 0{
                        sections.removeLast()
                        components.removeLast()
                    }else{i++}
                    
                    
                }
                
                //print(self.sig_dict)
            }
        }
        return (sections, components)
    }
    
    func formattedDict() -> [String: AnyObject]
    {
        for category in self.categories{
            var dic = (self.sig_dict[category] as! [[String : AnyObject]])[0]
            for (key,value) in dic{
                let type : ValueType = type_dict[category+key]!
                switch type{
                    
                case .Bool:
                    if(!(value is NSNull))
                    {
                        dic[key] = (value as! String == "True")
                    }
                case .String:
                    if value as! String == "*&*"
                    {
                        dic[key] = NSNull()
                        print("setting to null")
                    }
                    break
                case .Number:
                    if (value as! NSNumber).doubleValue < 0
                    {
                        dic[key] = NSNull()
                        print("setting to null")
                    }
                    break
                }
            }
            var list  = [[String:AnyObject]]()
            list.append(dic)
            self.sig_dict[category] = list
        }
        return self.sig_dict
    }
    
    func getMissingValues(category: String) -> [(String, ValueType)]
    {
        var output = [(String, ValueType)]()
        let cat_dic = self.sig_dict[category] as! [[String:AnyObject]]
        for (x,y) in cat_dic[0]
        {
            if y is NSNull
            {
                output.append((x, ValueType.Bool))
            }
            else if y is NSNumber
            {
                if (y as! NSNumber).doubleValue < 0
                {
                    output.append((x, ValueType.Number))
                }
            }
            else if y is String
            {
                if (y as! String)=="*&*"
                {
                    output.append((x, ValueType.String))
                }
            }
        }
        return output
    }
    
    func copy_dicts(category: String , new: [String:AnyObject])
    {
        var old = (sig_dict[category]  as! [[String: AnyObject]])[0]
        for (k,v) in new{
            print("Copying dicts")
            print(k,v)
            old[k] = v
        }
        var list  = [[String:AnyObject]]()
        list.append(old)
        sig_dict[category] = list
        
    }
    
    func update_sig(dict : AnyObject)
    {
        var dic = dict as! [String :AnyObject]
        for cat in ["medication", "frequency","event"]
        {
            if dic[cat] != nil{
                
                    let med_dict_new = (dic[cat]! as! [[String: AnyObject]])[0]
                    self.copy_dicts(cat, new: med_dict_new)
                
            }
        }
    }
    
    init(dic: AnyObject)
    {
        for x in self.categories{
            
            var list = [[String:AnyObject]]()
            var temp_dic = [String: AnyObject]()
            if (dic as! NSDictionary)[x] != nil
            {
                for a in (dic[x] as? NSArray)!{
                    for (key,value) in (a as?NSDictionary)!
                    {
                        
                        temp_dic[key as! String] = value
                        
                        let type_key = (x)+(key as! String)
                        var type = ValueType.String
                        if !(value is NSNull){
                            
                            if(value is NSNumber)
                            {
                                type = ValueType.Number
                            }
                            else
                            {
                                if((value as! String)=="True")
                                {
                                    type = ValueType.Bool
                                }
                                else if((value as! String)=="False")
                                {
                                    type = ValueType.Bool
                                }
                                else
                                {
                                    type = ValueType.String
                                }
                            }
                            
                        }
                        else
                        {
                            type = ValueType.Bool
                        }
                        type_dict[type_key]=type;
                    }
                    list.append(temp_dic)
                }
                sig_dict[x]=list
            }
            
        }
    }
    
    func detail_init(dic: AnyObject)
    {
        sig_dict["doctor_id"]=dic["doctor_id"] as! Int
        sig_dict["patient_id"]=dic["patient_id"] as! Int
        if  let last_taken = sig_dict["last_taken"] as? String{
            sig_dict["last_taken"]=DateTimeManager.instance.convertFromServerString(last_taken)
        }
        if  let last_taken = sig_dict["prescription_id"] as? String{
            sig_dict["prescription_id"]=DateTimeManager.instance.convertFromServerString(last_taken)
        }
        sig_dict["med_sig"]=dic["med_sig"] as! String
    }
    
}
