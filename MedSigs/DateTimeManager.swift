//
//  DateTimeManager.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/4/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import Foundation


class DateTimeManager
{



    static let instance = DateTimeManager()
    
    var DateFormatter = NSDateFormatter()

    init()
    {
    
       
    }

    func setToServerStyle()
    {
        DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //DateFormatter.timeZone = NSTimeZone(name: "UTC")
    }
    
    func convertFromServerString(string: String)->NSDate
    {
        setToServerStyle()
        return DateFormatter.dateFromString(string)!
    }
    
    func convertToServerStringWhole(date : NSDate) -> String
    {
        let in_components = NSCalendar.currentCalendar().components([.Hour, .Minute], fromDate: date)   
        let q_prime = NSCalendar.currentCalendar().dateBySettingHour(in_components.hour, minute: 0, second: 0, ofDate: date, options: NSCalendarOptions(rawValue: 0))
        
        setToServerStyle()
        return DateFormatter.stringFromDate(q_prime!)
    }
    
    func convertToServerString(date: NSDate) -> String
    {
        print(date)
        //gather date components from date
        let in_components = NSCalendar.currentCalendar().components([.Hour, .Minute], fromDate: date)
    
        let epoch_components: NSDateComponents = NSDateComponents()
        epoch_components.setValue(1, forComponent: NSCalendarUnit.Month);
        epoch_components.setValue(1, forComponent: NSCalendarUnit.Day);
        epoch_components.setValue(1970, forComponent: NSCalendarUnit.Year);
        
        let q = NSCalendar.currentCalendar().dateFromComponents(epoch_components)
        print(in_components.hour)
        print(in_components.minute)
        let q_prime = NSCalendar.currentCalendar().dateBySettingHour(in_components.hour, minute: in_components.minute, second: 0, ofDate: q!, options: NSCalendarOptions(rawValue: 0))
 
        
        setToServerStyle()
        let x = DateFormatter.stringFromDate(q_prime!)
        print(x)
        
        return x
    
    }
    
    func getTimeOnlyString(date: NSDate)->String
    {
        DateFormatter.timeZone = NSTimeZone.localTimeZone() //NSTimeZone.init(name: "EST") //
        DateFormatter.timeStyle = .ShortStyle
        DateFormatter.dateStyle = .NoStyle
        return DateFormatter.stringFromDate(date)
        
    }

}