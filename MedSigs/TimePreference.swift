//
//  TimePreference.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/3/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class TimePreference
{

    var description:String
    var food : Bool
    var wake : Bool
    var sleep: Bool
    var time : NSDate
    var time_id : Int
    
    
    init(dic: AnyObject)
    {
        description = dic["description"] as! String
        time = DateTimeManager.instance.convertFromServerString(dic["time"] as! String)
        time_id = dic["time_id"] as! Int
        food = dic["food"] as! Bool
        wake = dic["wake"] as! Bool
        sleep = dic["sleep"] as! Bool
    }

}
