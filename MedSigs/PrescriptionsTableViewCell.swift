//
//  PrescriptionTableViewCell.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/10/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class PrescriptionsTableViewCell: UITableViewCell {
    
    //MARK: Properties
    
    
    @IBOutlet weak var nameLabel: UILabel!
   
    @IBOutlet weak var subNameLabel: UILabel!
    var sig : Sig?
    static var height:CGFloat = 90
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setNameLabelText(text: String){
        self.nameLabel.text=text
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
