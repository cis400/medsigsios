//
//  APITests.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 2/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import XCTest


@testable import MedSigs

class APITests: XCTestCase{


    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRestCall(){
        let asyncExpectation = expectationWithDescription("longRunningFunction")
        
        
        
        RestApiManager.sharedInstance.putDoctor() { responseObject, error in
            // use responseObject and error here
            print("Response Handler")
            print("responseObject = \(responseObject); error = \(error)")
            asyncExpectation.fulfill()
            
        }
        self.waitForExpectationsWithTimeout(5) { error in
            print(error)
        }
        
    }
}