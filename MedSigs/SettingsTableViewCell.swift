//
//  SettingsTableViewCell.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/14/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell
{
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //nameLabel.translatesAutoresizingMaskIntoConstraints=true
        //nameLabel.frame=CGRectMake(200, 40, 24, 24)
    }


}
