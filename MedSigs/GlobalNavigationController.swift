//
//  GlobalNavigationController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/11/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

extension UIViewController {

    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

class GlobalNavigationController : UINavigationController{


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.barTintColor = UIColor(netHex: 0x4CAF50)
        self.navigationBar.titleTextAttributes=[NSForegroundColorAttributeName : UIColor.whiteColor()]
    }
    
}

class DoctorNavigationController : UINavigationController
{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.barTintColor = UIColor(netHex: 0x120D31)
        self.navigationBar.titleTextAttributes=[NSForegroundColorAttributeName : UIColor.whiteColor()]
    }

}
