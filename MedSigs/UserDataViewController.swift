//
//  UserDataViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/8/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class UserDataViewController : UIViewController
{
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func submitButtonPressed(sender: AnyObject) {
    
        
        if GlobalState.globalState.isDoctor{
            RestApiManager.sharedInstance.makeRequest(updateDataCallBack, prefixType: ReqPrefix.Update, pathType: ReqPath.DoctorData, args: ("doctor_id", GlobalState.globalState.doctor_id), ("doctor_name",nameTextField.text!))
        }
        else{
            RestApiManager.sharedInstance.makeRequest(updateDataCallBack, prefixType: ReqPrefix.Update, pathType: ReqPath.PatientData, args: ("patient_id", GlobalState.globalState.patient_id), ("patient_name",nameTextField.text!))
            print("Adding the patient name\n")
            print(nameTextField.text)
        }
    }
    
    func getDoctorData(doctor_id : Int)
    {
        RestApiManager.sharedInstance.makeRequest(getDoctorDataCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.DoctorData, args: ("doctor_id",doctor_id))
    }
    
    func getDoctorDataCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if error==nil{
            let dict = responseObject?.objectForKey("sol") as! NSArray
            GlobalState.globalState.name=dict[0]["doctor_name"] as! String
            performSegueWithIdentifier("segueToDoctorHome", sender: nil)
            
        }
    }
    
    func getPatientData(patient_id: Int)
    {
        RestApiManager.sharedInstance.makeRequest(getPatientDataCallBack, prefixType: ReqPrefix.Get, pathType: ReqPath.PatientData, args: ("patient_id",patient_id))
        
    }
    
    func getPatientDataCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->()
    {
        if error==nil{
            let dict = responseObject?.objectForKey("sol") as! NSArray
            GlobalState.globalState.name=dict[0]["patient_name"] as! String
             performSegueWithIdentifier("segueToSetDefaultTimePreferences", sender: nil)
        }
    }
    
        
    
    
    func updateDataCallBack(responseObject: NSDictionary?, error: NSError?, parameters: [String:AnyObject])->(){
        if error==nil
        {
            let dict = responseObject?.objectForKey("sol") as! NSArray
            if(dict.count==1)
            {
                if GlobalState.globalState.isDoctor{
                    self.getDoctorData(GlobalState.globalState.doctor_id)
                }
                else{
                    self.getPatientData(GlobalState.globalState.patient_id)
                }
                
            }
            else
            {
                print("error in creating the user")
            }
        }
        else
        {
            print("error in API call")
        }
        
    }


}
