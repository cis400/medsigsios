//
//  EventsManager.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class EventsManager
{

    var dict = [NSDate : [Event]]()
    

    func getFrontSchedule()->[(NSDate, String, [Event])]
    {
        print(dict)
        var output = [(NSDate, String, [Event])]()
        for (date , events) in self.dict
        {
            
            var name_list = String()
            for e in events
            {
               
                name_list=name_list+e.med_name+"\n"
            }
           
            let final_name_list=name_list.substringToIndex(name_list.endIndex.predecessor())
          
            output.append((date, final_name_list, events))
        }
        output.sortInPlace{ $0.0 == $1.0 ? true : $0.0.compare($1.0) == NSComparisonResult.OrderedAscending }
        print(output)
        return output
    }
    
    func getScheduleDetails(start_date : NSDate) -> ([String], [[Event]])
    {
        print("creating the schedule detail!!!")
        print(start_date)
        print(self.dict)
        var output_events = [[Event]]()
        var output_sections = [String]()
        var end_dic = [(NSDate) : [Event]]()
        for event in self.dict[start_date]!
        {
            var list = [Event]()
            list.append(event)
            if end_dic[event.end_time] != nil
            {
                list.appendContentsOf(end_dic[event.end_time]!)
            }
            end_dic[event.end_time] = list
        }
        var sorted = [(NSDate, [Event])]()
        for (end_time, event_list) in end_dic
        {
            sorted.append((end_time, event_list))
        }
        print(sorted)
        sorted.sortInPlace { $0.0 == $1.0 ? true : $0.0.compare($1.0) == NSComparisonResult.OrderedAscending }
        
        for (end_time, event_list) in sorted
        {
            var time_string = ""
            if(start_date.compare(end_time) == NSComparisonResult.OrderedSame){
                time_string = time_string + DateTimeManager.instance.getTimeOnlyString(start_date)
            }
            else{
                time_string = time_string + DateTimeManager.instance.getTimeOnlyString(start_date) + " - " + DateTimeManager.instance.getTimeOnlyString(end_time)
            }
            output_sections.append(time_string)
            output_events.append(event_list)
        }
        return (output_sections, output_events)
        
    }
    
    func addEvents(events : [Event])
    {
        for ev in events{
            var list = [Event]()
            list.append(ev)
            if self.dict[ev.start_time] != nil{
                list.appendContentsOf(self.dict[ev.start_time]!)
            }
            self.dict[ev.start_time]=list
        }
        
    }



}
