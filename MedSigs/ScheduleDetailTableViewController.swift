//
//  ScheduleDetailTableViewController.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 4/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class ScheduleDetailTableViewController : UITableViewController
{
    var events_manager : EventsManager?
    var start_date : NSDate?
    var sections = [String]()
    var section_contents = [[Event]]()
    
   
    
    override func viewDidLoad() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.sections.removeAll()
        self.section_contents.removeAll()
        super.viewDidLoad()
        self.title=DateTimeManager.instance.getTimeOnlyString(self.start_date!)
        let (sections, section_contents) = events_manager!.getScheduleDetails(self.start_date!)
        self.sections.appendContentsOf(sections)
        self.section_contents.appendContentsOf(section_contents)
        self.tableView.tableFooterView = UIView()
        tableView.reloadData()
        
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section_contents[section].count
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "ScheduleDetailTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ScheduleDetailTableViewCell
        
        let ev = section_contents[indexPath.section][indexPath.row]
        
        cell.instructionLabel.text = ev.instruction
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return sections[section]
    }

}
