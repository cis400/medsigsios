//
//  GlobalState.swift
//  MedSigs
//
//  Created by Brenden Guthrie on 3/13/16.
//  Copyright © 2016 Brenden Guthrie. All rights reserved.
//

import UIKit

class GlobalState
{
    
    
    static let globalState = GlobalState()
    
    var isDoctor: Bool
    var doctor_id: Int
    var patient_id: Int
    var name: String
    
    
    
    init()
    {
        self.isDoctor=false
        self.doctor_id=1
        self.patient_id=1
        self.name="Name"
    }
    
    
    func setData(isDoctor: Bool, doctor_id: Int, patient_id: Int)
    {
        self.isDoctor=isDoctor;
        if self.isDoctor{
            self.doctor_id=doctor_id;
            self.patient_id=0
        }
        else{
            self.patient_id=patient_id;
            self.doctor_id=0
        }
    
    }
  



}
